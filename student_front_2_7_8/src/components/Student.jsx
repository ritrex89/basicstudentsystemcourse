/**
 * To builf components quickly use the rafc snippet
 */

import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { Container } from '@mui/system';
import { FormGroup, Paper,Button } from '@mui/material';
import { useState, useEffect} from 'react';




export default function Student() {

    const [name, setName] = useState('')
    const [address, setaddress] = useState('')
    const [students, setStudent] = useState([])
    
    function handleSubmit(e) {
        e.preventDefault()
        const student={
            name,
            address
        }
        console.log(student)
        fetch('http://localhost/student/add',
                {method:'POST',
                headers:{'Content-Type':'application/json'},
                body:JSON.stringify(student)    
            })
            .then(resp=>console.log("New student added"))
            .catch(err=>console.log(err))
    }

    function getStudents() {
      
        fetch('http://192.168.56.1:8080/student/getAll')
            .then(resp=>resp.json()).then(res=>setStudent(res))
      
    }
    useEffect(()=>{getStudents()}, []) 
    
  return (
    <Box
      component="form"
      sx={{
        '& .MuiTextField-root': { m: 1, width: 600,
            display:'flex',
            flexFlow:'column',
            justifyContent:'center',
            alignItems:'center'
           },
      }}
      noValidate
      autoComplete="off"  
    >
        <Paper elevation={3} 
        >
            <h3 style={{color:'blue'}}> Add student</h3>
            
            <TextField
          required
          id="Student-Name"
          label="Student-Name"
          defaultValue={name}
          variant="filled"
          onChange={(e)=>setName(e.target.value)}
          fullWidth
        />
        <TextField
          id="Student-Address"
          label="Student-Address"
          defaultValue={address}
          variant="filled"
          fullWidth
          
          onChange={e=>setaddress(e.target.value)}
        />
        <Button variant='contained' color='secondary' 
        onClick={e=>{handleSubmit(e);getStudents()}}>
            Submit
        </Button>
        {/* <>
            {name}{address}
        </> */}
            <>
        
        
        <>
        {/* <TextField
          required
          id="filled-required"
          label="Required"
          defaultValue="Hello World"
          variant="filled"
        />
        <TextField
          disabled
          id="filled-disabled"
          label="Disabled"
          defaultValue="Hello World"
          variant="filled"
        />
        <TextField
          id="filled-password-input"
          label="Password"
          type="password"
          autoComplete="current-password"
          variant="filled"
        />
        <TextField
          id="filled-read-only-input"
          label="Read Only"
          defaultValue="Hello World"
          InputProps={{
            readOnly: true,
          }}
          variant="filled"
        />
        <TextField
          id="filled-number"
          label="Number"
          type="number"
          InputLabelProps={{
            shrink: true,
          }}
          variant="filled"
        />
        <TextField
          id="filled-search"
          label="Search field"
          type="search"
          variant="filled"
        />
        <TextField
          id="filled-helperText"
          label="Helper text"
          defaultValue="Default Value"
          helperText="Some important text"
          variant="filled"
        /> */}
        </> 
            </>
        </Paper>
        <Paper elevation={3}>
            {students.map(student=>(
            <Paper>Id:{student.id}
            Name:{student.name}
            Address:{student.address}
            </Paper>))}
        </Paper>
    </Box>
  );
}