import logo from './logo.svg';
import './App.css';
import ButtonAppBar from './components/AppBar';
import Student from './components/Student';

function App() {
  return (
    <div className="App">
      <ButtonAppBar></ButtonAppBar>
      <Student></Student>
    </div>
  );
}

export default App;
