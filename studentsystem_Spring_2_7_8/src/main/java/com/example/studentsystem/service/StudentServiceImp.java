package com.example.studentsystem.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.studentsystem.model.Student;
import com.example.studentsystem.repository.StudentRepository;


@Service
public class StudentServiceImp implements StudentService {
    @Autowired
    private StudentRepository studentRepo;
    @Override
    public Student saveStudent(Student student){
        return studentRepo.save(student);
    }

    @Override
    public List<Student> getAllStudents() {
        return studentRepo.findAll();
    }
}
