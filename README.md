
# Systema de estudiantes 

Proyecto del curso Fullstack Application Springboot React

Un sistema sencillo de creacion y consulta de estudiantes.

https://www.udemy.com/share/104VRW3@rJnqV0DaOtVYwxOgJuJX2uBa60m20PMrcEKY-qxopId1F6YFi6KZruwUBdwAXaYNDA==/




## Warning
Los CORS del proyecto estn completamente desprotegidos. 

Si se desea probar localmente, no hay problema. 

Pero eventualmente se tendrá que limitar el acceso a la API.
## Tech Stack

**Server:** Springboot 2.7.8, WAMPP, maven
**Client:** React, material ui

        
        
## Run Locally

Clona el proyecto

```bash
  git clone https://link-to-project
```

Entra al directorio del proyecto

```bash
  cd "studensystem_2_7_8_fullstack"
```
Para el **Frontend** entra en la carpeta:

```bash
  cd "student_front_2_7_8"
```
Instala las dependencias
```bash
  npm install
```

Inicia el servidor

```bash
  npm run start
```

Para el **Backend** necesitaras hacer lo siguiente:

Entra al directorio del Backend

```bash
  cd "studentsystem_Spring_2_7_8"
```
Instala maven si todavia no lo has hecho.

Una vez instalado, instala las dependencias:

```bash
  mvn dependency:resolve
```

Instala XAMPP (https://www.apachefriends.org/es/index.html) y ejecutalo. En particular deben estar corriendo Apache y MySQL. Con Tomcat el unico inconvenente de tenerlo ejecutando es que puede que ocupe el puerto que Spring necesite usar.

Una vez hecho eso ejecuta el servidor.

```bash
    mvn spring-boot:run
```

## 🛠 Skills
Javascript, HTML, CSS, MySQL, Springboot, React, Material UI

